package com.extrawest.test.antonkhomyuk.callboard.controllers;

import com.extrawest.test.antonkhomyuk.callboard.domain.Advt;
import com.extrawest.test.antonkhomyuk.callboard.repos.AdvtRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
public class CallboardController {
    @Autowired
    private AdvtRepo advtRepo;

    @GetMapping("/")
    public String greeting(Map<String, Object> model) {
        return "main";
    }

    @GetMapping("/home")
    public String main(Map<String, Object> model){
        Iterable<Advt> advts = advtRepo.findAll();
        model.put("advts", advts);
        return "home";
    }

    @PostMapping("/home")
    public String add(@RequestParam String text, @RequestParam String tag, Map<String, Object> model){
        Advt advt = new Advt(text, tag);

        advtRepo.save(advt);

        Iterable<Advt> advts = advtRepo.findAll();

        model.put("advt", advts);

        return "home";
    }

    @PostMapping("filter")
    public String filter(@RequestParam String filter, Map<String, Object> model){
        Iterable<Advt> advts;
        if(filter != null && !filter.isEmpty()){
            advts = advtRepo.findByTag(filter);
        }else{
            advts = advtRepo.findAll();
        }
        model.put("advt", advts);
        return "home";
    }

}