package com.extrawest.test.antonkhomyuk.callboard.repos;

import com.extrawest.test.antonkhomyuk.callboard.domain.Advt;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AdvtRepo extends CrudRepository<Advt, Long> {
    List<Advt> findByTag(String tag);
}
